package Lesson2;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Scanner;

import java.util.HashMap;

class Lesson3_1 {
    public static void main(String[] args) {
        int age;
        age = 20;
        System.out.println(age);
        byte b;
        b = 50;
        System.out.println(b);
        short s;
        s = 1000;
        System.out.println(s);
        long l;
        l = 1234567890L;
        System.out.println(l);
        float euro;
        euro = 75.50F;
        System.out.println(euro);
        double pi;
        pi = 3.14159;
        System.out.println(pi);
        char name;
        name = 's';
        System.out.println(name);
        boolean k = true;
        System.out.println(b);
        String a = "Hello";
        String c = "World";
        System.out.println(a + " " + c);
    }
}

class Lesson4 {
    public static void main(String[] args) {
        int user_value;
        user_value = 50;
        boolean a;
        if ((user_value == 100) || (user_value == 500) || (user_value == 0)) a = true;
        else a = false;
        System.out.println(a);
    }
}

class Lesson5_1 {
    public static void main(String[] args) {
        System.out.println("Enter number");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        System.out.println("Your entered number: " + num);

        vremenaGoda(num);
    }

    private static void vremenaGoda(int number) {
        for (int i = 0; i < 4; i++) {
            if (i == number) {
                if (i == 0) {
                    System.out.println("Zima");
                } else if (i == 1) {
                    System.out.println("Vesna");
                } else if (i == 2) {
                    System.out.println("Leto");
                } else if (i == 3) {
                    System.out.println("Osen");
                }
            }
        }
    }
}


class Lesson5_2 {
    public static void main(String[] args) {
        System.out.println("Enter number");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        System.out.println(compare(a, b, c));
    }

    private static boolean compare(int a, int b, int c) {
        if (a < b && b < c) {
            return true;
        } else return false;
    }
}

class Lesson5_3 {
    public static void main(String[] args) {
        System.out.println("Enter number");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        System.out.println("Your entered number: " + num);

        perevertish(num);
    }

    private static void perevertish(int number) {
        while (number != 0) {
            System.out.print(number % 10);
            number = number / 10;
        }
    }
}

class Lesson5_4 {
    public static void main(String[] args) {
        int sum;
        sum = 0;
        int count;
        count = 0;

        for (int i = 1; i < 1000; i++) {
            if (i % 3 == 0) {
                sum = sum + i;
                count++;
            }
            if (count == 10) {break;}
        }
        System.out.println(sum);
    }
}

class Lesson5_5 {
    public static void main(String[] args) {
        System.out.println(getCountsOfDigits(1000L));
        System.out.println(getCountsOfDigits(10000L));
    }

    public static int getCountsOfDigits(final long number) {
        return String.valueOf(number).length();
    }
}

class Lesson5_6 {
    public static void main(String[] args) {
        long test = 128;
        if (isPowersOfTwo(test)) {
            double ans = getPowersOfTwo(test);
            System.out.println("Число " + test + " степень двойки. Степень: " + ans);
        } else {
            System.out.println("Число " + test + " не степень двойки");
        }
    }

    public static boolean isPowersOfTwo(long number) {
        number = Math.abs(number); //чтобы избавится от проблем с отрицательынм числами
        return Long.bitCount(number) == 1;
    }

    public static double getPowersOfTwo(double number) {
        number = Math.abs(number); //чтобы избавится от проблем с отрицательынм числами
        return Math.log(number) / Math.log(2);
    }
}

class Lesson5_7 {
            private static HashMap<Long, String> epithet = new HashMap<Long, String>() {{
                put(0L, "Радиоактивный");
                put(1L, "Гречневый");
                put(2L, "Демонический");
                put(3L, "Призрачный");
                put(4L, "Озорной");
                put(5L, "Призрачный");
                put(6L, "Космический");
                put(7L, "Звёздный");
                put(8L, "Сексуальный");
                put(9L, "Непобедимый");
            }};

            private static HashMap<Long, String> name = new HashMap<Long, String>() {{
                put(0L, "Капитан");
                put(1L, "Эльф");
                put(2L, "Пингвин");
                put(3L, "Индеец");
                put(4L, "Гном");
                put(5L, "Упырь");
                put(6L, "Бородач");
                put(7L, "Бобёр");
                put(8L, "Король");
                put(9L, "Толстопуз");
                put(10L, "Киллер");
            }};

            public static void main(String[] args) {
                String epithet = getHeroicEpithet(9);
                String name = getHeroicName(10);
                System.out.println(epithet + " " + name);
            }

            private static String getHeroicEpithet(int maxNumber) {
                return epithet.get(Math.round(Math.random() * maxNumber));
            }

            public static String getHeroicName(int maxNumber) {
                return name.get(Math.round(Math.random() * maxNumber));
            }

        }